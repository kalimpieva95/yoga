import "./TextButton.css";

export const TextButton = (props) => {
  return <button className={"textButton"}>{props.children}</button>;
};
