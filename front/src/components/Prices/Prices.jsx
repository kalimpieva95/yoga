import Button from "@mui/material/Button";
import React from "react";
import { FormattedMessage } from "react-intl";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useNavigate } from "react-router-dom";
import { practicesType } from "../../data/index";
import { getCheckoutUrl } from "../../firebase";
import { isLoggedIn } from "../../storage/session";
import "./Prices.css";

export default function Prices({ responsive }) {
  const navigate = useNavigate();

  const onClick = async (price, counter) => {
    if (!isLoggedIn()) {
      navigate("/login");
    } else {
      await getCheckoutUrl(price, counter);
    }
  };
  return (
    <div>
      <div className="pricesTitleBox">
        <p className="descriptor">
          <FormattedMessage id="descriptorPrices" />
        </p>
        <p className="titlePrices">
          <span>
            <FormattedMessage id="titlePricesFirst" />
          </span>
          <span className="titlePricesColor">
            <FormattedMessage id="titlePricesColor" />
          </span>
          <FormattedMessage id="titlePrices" />
        </p>
      </div>
      <Carousel
        swipeable={true}
        draggable={false}
        responsive={responsive}
        ssr={false}
        infinite={true}
        autoPlay={false}
        partialVisible={true}
        keyBoardControl={true}
        containerClass="carousel-container"
        itemClass="priceCard"
        removeArrowOnDeviceType={["mobile"]}
      >
        {practicesType.map((practice) => (
          <div key={"practice" + practice.id} className="priceCardContent">
            <div className="priceCardText">
              <span className="priceCardTitle">
                <FormattedMessage id={practice.title} />
              </span>
              <span className="priceCardFirstText">
                <FormattedMessage id={practice.title + "Duration"} />
              </span>
              <span className="priceCardSecondText">
                <FormattedMessage id={practice.title + "Price"} />
              </span>
            </div>
            <div className="buttonInPriceCardContainer">
              <Button
                onClick={() => onClick(practice.id, practice.counter)}
                size="small"
                variant="contained"
                className="buttonInPriceCard"
              >
                <FormattedMessage id={"buttonInPriceCard"} />
              </Button>
            </div>
          </div>
        ))}
      </Carousel>
    </div>
  );
}
