export * from "./sections";
export * from "./practices";
export * from "./footerNavigation";
export * from "./toursText";
export * from "./advantages";
export * from "./course";
export * from "./prices";
